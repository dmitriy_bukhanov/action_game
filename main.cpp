#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow window;
      window.setWindowTitle("QT OpenGL");
      window.setGeometry(100,100,600,600); // Смещение и положение окна
      window.show();
      window.setMouseTracking(true); // вызывать метод mouseMoveEvent при изменении позиции указателя, даже без клика кнопкой мыши
      window.setCursor(QCursor(Qt::BlankCursor));

    return a.exec();
}
