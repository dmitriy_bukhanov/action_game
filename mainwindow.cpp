#include "mainwindow.h"


MainWindow::MainWindow(QWidget* parent) : QGLWidget(parent)
{
    x = 0;
    y = 0;
    z = 1;
}

void MainWindow::initializeGL()
{
     glClearColor( 9.0f, 1.0f, 0.9f, 1.0f );
}

void MainWindow::resizeGL(int width, int height)
{
    glViewport( 0, 0, width, height );

   // Reset coordinate system
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();

   // Establish clipping volume (left, right, bottom, top, near, far)
    glOrtho( -200, 200, -200, 200, 200.0, -200.0);

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
}

void MainWindow::paintGL()
{
    glClear( GL_COLOR_BUFFER_BIT );

      // Выставляем цвет рисования
      glColor3f( 1.0f, 0.0f, 0.0f );

      // Выставляем размер точки
      glPointSize( 5 );

      // Рисуем точки
      glBegin( GL_POLYGON );
      {
          glVertex3f( -50, 50, 0);
          glVertex3f( 50, 50 , 0);
          glVertex3f( 50, -50, -20);
          glVertex3f( -50, -50, -30);
      }
      glEnd();
}

void MainWindow::keyPressEvent(QKeyEvent *ke)
{
    switch (ke->key())
    {
        case Qt::Key_Space:
            std::cout<< " Нажат пробел";
            break;
        case Qt::Key_Up:
            std::cout << "Вперед";
            break;
    }
    updateGL();
}

void MainWindow::mouseMoveEvent(QMouseEvent *me)
{
    updateGL();
}

void MainWindow::mousePressEvent(QMouseEvent *me)
{
}

void MainWindow::mouseReleaseEvent(QMouseEvent *me)
{
}
